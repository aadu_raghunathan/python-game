# Python game

Iss Assignment 3 game

How to play:
This game is a 2 player game where the main objective is to  take control of a ship and navigate across a river full of icebergs and dangerous crocodiles and reach the other end. 

Controls:
For both players, the arrow keys control the movement of the players

Scoring system:

- A player is awarded 5 points for crossing an iceberg
- 10 points for crossing a crocodile
- 5,10,and 15 points for reaching the safety platforms
- 50 points for crossing the river and reaching the next level
- A players score is reduced based on how long it takes them to cross the river

At the end of the game , the player with the higher score wins.
May the best sailor win.
