from config import *

# This class defines the stationary enemies in the game


class enemy_box_static(object):
    def __init__(self, x, y, len, bred):
        self.x = x  # x- coordinate
        self.y = y  # y- coordinate
        self.len = len  # length of the box
        self.bred = bred  # breadth of the box

    # This function creates the enemy object and places it on the screen
    def make_enemy(self):
        global img1
        screen.blit(img1, (self.x, self.y))

# This function checks for collision of player 1 with the objects on the screen


def check_collision1(box):
    global x1
    global y1
    global lives
    global flags1
    global time
    # check for overlap between player and object
    if(x1-box.x > -width1 and y1-box.y > -height1 and x1-box.x < 50 and y1-box.y < 50):
        x1 = 400  # Sends the player back to the starting point
        y1 = 780
        lives[0] = lives[0]-1  # decrementing the number of lives for the player
        flags1[0] = 0  # Resetting the flags for crossing the platforms
        flags1[1] = 0
        flags1[2] = 0
        time = 0  # restarting the time variable
        print_hit()

# This function works the same way for player 2
# Checks for collisions and sends the player to the staring point


def check_collision2(box):
    global x2
    global y2
    global lives
    global flags2
    global time
    if(x2-box.x > -width2 and y2-box.y > -height2 and x2-box.x < 50 and y2-box.y < 50):
        x2 = 400
        y2 = 0
        lives[1] = lives[1]-1
        flags2[0] = 0
        flags2[1] = 0
        flags2[2] = 0
        time = 0
        print_hit()

# Class to create the moving enemies
# These enemies have 5 parameters x,y,length,breadth,velocity


class enemy_box_dynamic(object):
    def __init__(self, x, y, len, bred, velocity):
        self.x = x
        self.y = y
        self.len = len
        self.bred = bred
        self.velocity = velocity
# This function creates the enemies and places them on the screen

    def make_move(self):
        global img
        screen.blit(img, (self.x, self.y))


# the variable running checks whether the program is still running
running = True
# the variable flag decides which player is playing
flag = 0
# The below for loop gets 4 random velocities and assigns them to the moving objects
for i in range(0, 4):
    x.append(random.randrange(0, 750))
    temp = random.randrange(1, 20)
    move_vel.append(temp)
    move_vel2.append(temp)
# Creating the 4 moving objects
move1 = enemy_box_dynamic(x[0], 710, 50, 50, move_vel[0])
moving_objects.append(move1)
move2 = enemy_box_dynamic(x[1], 420, 50, 50, move_vel[1])
moving_objects.append(move2)
move3 = enemy_box_dynamic(x[2], 260, 50, 50, move_vel[2])
moving_objects.append(move3)
move4 = enemy_box_dynamic(x[3], 90, 50, 50, move_vel[3])
moving_objects.append(move4)
# To increase the speed of the moving objects, I used the variable increment velocity
increment_velocity = random.randrange(5, 10)
# Game loop
while running:
    py.time.delay(100)
    # updating the time variable. This will be used to update the score based on time
    time = time - 0.01
    # The next for loop is used to get the moving objects to move along the screen
    for i in range(0, 4):
        if flag == 1:
            moving_objects[i].x += move_vel2[i]
        else:
            moving_objects[i].x += move_vel[i]
        if moving_objects[i].x > 800:
            moving_objects[i].x = 0
    # closes the game if exit button is pressed
    for event in py.event.get():
        if event.type == py.QUIT:
            running = False
    # for loop to assign random x postions to the stationary objects
    for i in range(0, 4):
        x_stat.append(random.randrange(0, 750))
    # creating the stationary objects
    stat1 = enemy_box_static(x_stat[0], 620, 50, 50)
    stationary_objects.append(stat1)
    stat2 = enemy_box_static(x_stat[1], 490, 50, 50)
    stationary_objects.append(stat2)
    stat3 = enemy_box_static(x_stat[2], 330, 50, 50)
    stationary_objects.append(stat3)
    stat4 = enemy_box_static(x_stat[3], 30, 50, 50)
    stationary_objects.append(stat4)
    # if the flag = 0, then it implies player 1 is playing
    if flag == 0:
        # adding movement
        arrow = py.key.get_pressed()

        if arrow[py.K_LEFT]:
            if x1 > 0:
                x1 -= vel
        if arrow[py.K_RIGHT]:
            if x1 < 780:
                x1 += vel
        if arrow[py.K_UP]:
            if y1 > 0:
                y1 -= vel
        if arrow[py.K_DOWN]:
            if y1 < 780:
                y1 += vel
        screen.fill((0, 0, 0))
        # creating the game screen with the partitions
        py.draw.rect(screen, (255, 0, 0), (0, 780, 800, 20))
        py.draw.rect(screen, (0, 0, 255), (0, 600, 800, 180))
        py.draw.rect(screen, (255, 0, 0), (0, 580, 800, 20))
        py.draw.rect(screen, (0, 0, 255), (0, 400, 800, 180))
        py.draw.rect(screen, (255, 0, 0), (0, 380, 800, 20))
        py.draw.rect(screen, (0, 0, 255), (0, 200, 800, 180))
        py.draw.rect(screen, (255, 0, 0), (0, 180, 800, 20))
        py.draw.rect(screen, (0, 0, 255), (0, 20, 800, 160))
        py.draw.rect(screen, (255, 0, 0), (0, 0, 800, 20))
        # generating the player character
        screen.blit(img2, (x1, y1))
        # checking for collisions with all the objects
        for i in range(0, 4):
            check_collision1(moving_objects[i])
            check_collision1(stationary_objects[i])
        # the following section is used to display text on the screen
        # score,lives,level,active player are displayed
        txt = font.render('Active Player : 1 ', True, py.Color('white'))
        screen.blit(txt, (0, 800))
        txt1 = font.render('Score : ', True, py.Color('white'))
        txt2 = font.render(str(int(score[0])), True, py.Color('white'))
        screen.blit(txt1, (0, 850))
        screen.blit(txt2, (75, 850))
        txt3 = font.render('Lives : ', True, py.Color('white'))
        txt4 = font.render(str(lives[0]), True, py.Color('white'))
        text = font.render('Level :', True, py.Color('white'))
        text1 = font.render(str((level[0])), True, py.Color('white'))
        screen.blit(txt3, (0, 900))
        screen.blit(txt4, (75, 900))
        screen.blit(text, (0, 930))
        screen.blit(text1, (75, 930))
        # Scoring
        for i in range(0, 4):
            if y1 < y_stat[i] and flags1_crossed_stat[i] == 0:
                score[0] += 5
                flags1_crossed_stat[i] = 1
            if y1 < y_move[i] and flags1_crossed_move[i] == 0:
                score[0] += 10
                flags1_crossed_move[i] = 1
        # y1 == 0 if player 1 completes the level
        if y1 == 0:
            score[0] += 50
            x1 = 400
            y1 = 780
            flags1[0] = 0
            flags1[1] = 0
            flags1[2] = 0
            time = 0
            level[0] += 1
            print_level_up()
            # after clearing a level, the velocity of the moving objects is increased
            for i in range(0, 4):
                move_vel[i] = move_vel[i] + increment_velocity
        elif y1 == 180 and flags1[2] != 1:
            score[0] += 15
            flags1[2] = 1
        elif y1 == 380 and flags1[1] != 1:
            score[0] += 10
            flags1[1] = 1
        elif y1 == 580 and flags1[0] != 1:
            score[0] += 5
            flags1[0] = 1
        # using time to update the score
        score[0] += time
        for i in range(0, 4):
            moving_objects[i].make_move()
            stationary_objects[i].make_enemy()
        # If the number of lives left go to 0,all parameters reset and player 2 plays
        if lives[0] == 0:
            flag = 1
            time = 0
    # player 2
    if flag == 1:
        # adding movement for player 2
        arrow = py.key.get_pressed()
        if arrow[py.K_LEFT]:
            if x2 > 0:
                x2 -= vel
        if arrow[py.K_RIGHT]:
            if x2 < 780:
                x2 += vel
        if arrow[py.K_UP]:
            if y2 > 0:
                y2 -= vel
        if arrow[py.K_DOWN]:
            if y2 < 780:
                y2 += vel
        screen.fill((0, 0, 0))
        # assigning velocities to the moving objects
        for i in range(0, 4):
            moving_objects[i].velocity = move_vel2[i]
        # creating the game screen
        py.draw.rect(screen, (255, 0, 0), (0, 780, 800, 20))
        py.draw.rect(screen, (0, 0, 255), (0, 600, 800, 180))
        py.draw.rect(screen, (255, 0, 0), (0, 580, 800, 20))
        py.draw.rect(screen, (0, 0, 255), (0, 400, 800, 180))
        py.draw.rect(screen, (255, 0, 0), (0, 380, 800, 20))
        py.draw.rect(screen, (0, 0, 255), (0, 200, 800, 180))
        py.draw.rect(screen, (255, 0, 0), (0, 180, 800, 20))
        py.draw.rect(screen, (0, 0, 255), (0, 20, 800, 160))
        py.draw.rect(screen, (255, 0, 0), (0, 0, 800, 20))
        # making the player
        screen.blit(img2, (x2, y2))
        for i in range(0, 4):
            stationary_objects[i].make_enemy()
        # scoring
        for i in range(0, 4):
            if y2 > y_stat[i]+50 and flags2_crossed_stat[i] == 0:
                score[1] += 5
                flags2_crossed_stat[i] = 1
            if y2 > y_stat[i]+50 and flags2_crossed_move[i] == 0:
                score[1] += 10
                flags2_crossed_move[i] = 1
        # if player 2 wins a level, update the level
        if y2 == 780:
            score[1] += 50
            x2 = 400
            y2 = 0
            flags2[0] = 0
            flags2[1] = 0
            flags2[2] = 0
            time = 0
            level[1] += 1
            print_level_up()
            # increase velocity of moving objects
            for i in range(0, 4):
                move_vel2[i] = move_vel2[i] + increment_velocity
        elif y2 == 180 and flags2[2] != 1:
            score[1] += 5
            flags2[2] = 1
        elif y2 == 380 and flags2[1] != 1:
            score[1] += 10
            flags2[1] = 1
        elif y2 == 580 and flags2[0] != 1:
            score[1] += 15
            flags2[0] = 1
        score[1] += time
        for i in range(0, 4):
            check_collision2(moving_objects[i])
            check_collision2(stationary_objects[i])
        # Printing necessary information on the screen
        # lives,level,score
        txt = font.render('Active Player : 2 ', True, py.Color('white'))
        screen.blit(txt, (0, 800))
        txt1 = font.render('Score : ', True, py.Color('white'))
        txt2 = font.render(str(int(score[1])), True, py.Color('white'))
        screen.blit(txt1, (0, 850))
        screen.blit(txt2, (75, 850))
        txt3 = font.render('Lives : ', True, py.Color('white'))
        txt4 = font.render(str(lives[1]), True, py.Color('white'))
        text = font.render('Level :', True, py.Color('white'))
        text1 = font.render(str((level[1])), True, py.Color('white'))
        screen.blit(txt3, (0, 900))
        screen.blit(txt4, (75, 900))
        screen.blit(txt3, (0, 900))
        screen.blit(txt4, (75, 900))
        screen.blit(text, (0, 930))
        screen.blit(text1, (75, 930))
        for i in range(0, 4):
            moving_objects[i].make_move()
            stationary_objects[i].make_enemy()
        # when player 2 loses all 3 lives
        if lives[1] == 0:
            py.display.quit()
            screen2 = py.display.set_mode((800, 920))
            screen2.fill((255, 255, 255))
            # Displaying the winner
            txt5 = font.render('The result is', True, py.Color('black'))
            if score[0] > score[1]:
                txt6 = font.render('Player 1 wins', True, py.Color('black'))
            elif score[1] > score[0]:
                txt6 = font.render('Player 2 wins', True, py.Color('black'))
            else:
                txt6 = font.render('Tie', True, py.Color('black'))
            screen2.blit(txt5, (300, 400))
            screen2.blit(txt6, (430, 400))
            py.time.delay(2000)

    py.display.update()