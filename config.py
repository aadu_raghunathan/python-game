import pygame as py
import random

py.init()
# font for displaying text
font = py.font.SysFont("comicsansms", 30)

# making the screen
screen = py.display.set_mode((800, 950))

# loading images for the objects
img = py.image.load('crocodile_png13182.png')
img1 = py.image.load('iceberg.png')
img2 = py.image.load('ship.png')

# lists of moving and stationary objects
moving_objects = []
stationary_objects = []

x = []
x_stat = []
# here x1,y1,height1 and width1 are parameters of player1
x1 = 400
y1 = 780
width1 = 20
height1 = 20
vel = 10
# here x2,y2,height2 and width2 are parameters of player2
x2 = 400
y2 = 0
width2 = 20
height2 = 20
# list of the lives each player has
lives = [3, 3]

score = [0, 0]

move_vel = []

move_vel2 = []

# flags to check if platforms have been croosed
flags1 = [0, 0, 0]

flags2 = [0, 0, 0]

# y coordinates of the stationary and moving objects
y_stat = [30, 330, 490, 620]

y_move = [90, 260, 420, 710]

# flags to check if objects have been crossed

flags1_crossed_stat = [0, 0, 0, 0]

flags1_crossed_move = [0, 0, 0, 0]

flags2_crossed_stat = [0, 0, 0, 0]

flags2_crossed_move = [0, 0, 0, 0]

time = 0

level = [1, 1]
# function to display alert based on hitting an obstacle


def print_hit():
    screen.fill((255, 255, 255))
    hit = font.render('You got hit', True, (0, 0, 0))
    screen.blit(hit, (400, 400))
    py.display.update()
    py.time.delay(2000)
# function to display alert based on levelling up


def print_level_up():
    screen.fill((255, 255, 255))
    level_up = font.render(
        'congratulations. You have levelled up', True, (0, 0, 0))
    screen.blit(level_up, (200, 400))
    py.display.update()
    py.time.delay(2000)
